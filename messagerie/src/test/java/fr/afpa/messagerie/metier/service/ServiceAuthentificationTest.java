package fr.afpa.messagerie.metier.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.services.ServiceAuthentification;

public class ServiceAuthentificationTest {
	
	@Test
	public void testRechercheByLoginOrNom() {
		Personne personne = new ServiceAuthentification().authentification("admin", "1234");
		assertEquals("THEBOSS", personne.getNom());
		assertEquals(1, personne.getRole().getId());
		personne = new ServiceAuthentification().authentification("adz", "1234");
		assertNull(personne);
	}

}
