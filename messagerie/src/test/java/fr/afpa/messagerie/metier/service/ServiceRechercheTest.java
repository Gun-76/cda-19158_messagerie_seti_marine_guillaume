package fr.afpa.messagerie.metier.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.services.ServiceRecherche;

public class ServiceRechercheTest {
	
	
	
	@Test
	public void testGetAllPersonnes() {
		List<Personne> liste = new ServiceRecherche().getAllPersonnes();
		assertFalse(liste.isEmpty());
	}

}
