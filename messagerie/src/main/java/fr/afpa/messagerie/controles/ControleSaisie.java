package fr.afpa.messagerie.controles;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.afpa.messagerie.dao.persistance.ServiceAuthentificationDao;

public class ControleSaisie {
	
	

	private ControleSaisie() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Comtrole si le nom ou le prenom est valide
	 * 
	 * @param nom : le nom a controler
	 * @return true si le nom est valide
	 */
	public static boolean isNom(String nom) {
		return nom.matches("^[A-Za-z][A-Za-z- ]*$");
	}

	/**
	 * Controle si le mail est vailde
	 * 
	 * @param mail mail a controler
	 * @return true si le mail est valide, false sinon
	 */
	public static boolean isMail(String mail) {
		String regexMailPartie1 = "\\w(\\.?[\\w-]+)*";
		String regexMailPartie2 = "[\\w&&[^_]]{3,13}";
		String regexMailPartie3 = "[\\w&&[^_]]{2,3}";
		String rejexMail = "^" + regexMailPartie1 + "@" + regexMailPartie2 + "\\." + regexMailPartie3 + "$";
		return mail.matches(rejexMail);
	}

	/**
	 * controle si la date de naissance est valide
	 * 
	 * @param date date à controller
	 * @return true si la chaine est une date valide, false sinon
	 */
	public static boolean isDateValide(String date) {

		try {
			LocalDate date2 = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			return true;
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}

	}

	/**
	 * controle si le telephone est valide (10 chiffres sans espace)
	 * 
	 * @param tel le numero à controller
	 * @return true si numero est valide, false sinon
	 */
	public static boolean isNumTel(String tel) {
		
		return tel.matches("\\d{10}");
	}
	
	/**
	 * methode qui verifie si la chaine de caractere et non vide (les espaces sont concidéré comme vide)
	 * @param chaine chaine de caractère à vérifier
	 * @return true si la chaine de caractère est non vide et n'est pas constitué que des espaces, false sinon
	 */
	public static boolean isNonVide(String chaine) {
		return  chaine.replaceAll(" ", "").length()>0;
			
	}

	public static boolean isLogin(String login) {
		return !(login.contains(" ")|| login.contains(","));
	}


}
