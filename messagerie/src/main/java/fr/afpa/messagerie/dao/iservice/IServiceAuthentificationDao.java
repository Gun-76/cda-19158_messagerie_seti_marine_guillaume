package fr.afpa.messagerie.dao.iservice;

import fr.afpa.messagerie.dao.entites.AuthentificationDao;

public interface IServiceAuthentificationDao {
	
	/**
	 * demande à la bdd si le login et le mot de passe sont corrects, si oui renvoie de l'authentificationDaO correspondant
	 * @param authDaoVerif : l'authentification à vérifier
	 * @return : une entité authentificationDAO si le login et le mot de passe sont corrects, null sinon
	 */
	public AuthentificationDao authentification(AuthentificationDao authDaoVerif);

}
