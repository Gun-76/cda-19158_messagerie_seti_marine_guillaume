package fr.afpa.messagerie.dao.entites;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor

@Getter
@Setter
@ToString(of= {"id","nom","prenom","mail","tel","adresse","dateDeNaissance","role","authentification"})

@Entity
@Table(name = "personne")


@NamedQuery(name = "rechercheByLogin",query = "from PersonneDao where login = :login")
@NamedQuery(name = "getAll", query = "from PersonneDao")
public class PersonneDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "personne_generator")
	@SequenceGenerator(name = "personne_generator", sequenceName = "seq_personne",allocationSize = 1,initialValue = 4)
	@Column (name="id_personne", updatable = false, nullable = false )
	int id;
	
	@Column (name="nom", updatable = true, nullable = false,length = 25)
	String nom;
	@Column (name="prenom", updatable = true, nullable = false,length = 25)
	String prenom;
	@Column (name="mail", updatable = true, nullable = false,length = 50)
	String mail;
	@Column (name="tel", updatable = true, nullable = false,length = 10)
	String tel;
	@Column (name="adresse", updatable = true, nullable = false,length = 255)
	String adresse;
	@Column (name="date_naissance", updatable = true, nullable = false)
	LocalDate dateDeNaissance;
	@Column (name="actif", updatable = true, nullable = false)
	boolean actif;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_role")
	RoleDao role;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="login",unique = true)
	AuthentificationDao authentification;
	
	@OneToMany(mappedBy="personneExp")
	List<MessageDao> listeMessageEnvoyes;
	
	@OneToMany(mappedBy="personneDest")
	List <MessageDao> listeMessageRecus;
	
	

	public PersonneDao(String nom, String prenom, String mail, String tel, String adresse, LocalDate dateDeNaissance,
			boolean actif) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.tel = tel;
		this.adresse = adresse;
		this.dateDeNaissance = dateDeNaissance;
		this.actif = actif;
	}
	
	
	
}
