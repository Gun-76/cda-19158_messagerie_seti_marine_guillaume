package fr.afpa.messagerie.dao.persistance;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.AuthentificationDao;
import fr.afpa.messagerie.dao.iservice.IServiceAuthentificationDao;
import fr.afpa.messagerie.utils.HibernateUtils;

@Component
public class ServiceAuthentificationDao implements IServiceAuthentificationDao{

	@Override
	public AuthentificationDao authentification(AuthentificationDao authDaoVerif) {
		Session s = null;
		AuthentificationDao authDaoResultat = null;
		
		try {
			s = HibernateUtils.getSession();
			Query q = s.getNamedQuery("verifAuth");
			q.setParameter("login", authDaoVerif.getLogin());
			q.setParameter("mdp", authDaoVerif.getMdp());
			authDaoResultat = (AuthentificationDao) q.getSingleResult();
			
		}catch (Exception e) {
		Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return authDaoResultat;
	}

}
