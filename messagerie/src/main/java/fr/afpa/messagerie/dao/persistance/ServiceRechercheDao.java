package fr.afpa.messagerie.dao.persistance;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.MessageDao;
import fr.afpa.messagerie.dao.entites.PersonneDao;
import fr.afpa.messagerie.dao.iservice.IServiceRechercheDao;
import fr.afpa.messagerie.utils.HibernateUtils;

@Component
public class ServiceRechercheDao implements IServiceRechercheDao {

	
	@Override
	public List<PersonneDao> getAllPersonnes() {
		Session s = null;
		List<PersonneDao> listePersDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAll");
			if (q != null) {
				listePersDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listePersDao;
	}

	@Override
	public PersonneDao rechercheLogin(String login) {
		Session s = null;
		PersonneDao personneDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
				q = s.getNamedQuery("rechercheByLogin");
				q.setParameter("login", login);
			
			
				personneDao = (PersonneDao) q.getSingleResult();
				List<MessageDao> listeMessage = transformationListeNonPersistantBag(
						personneDao.getListeMessageEnvoyes());
				personneDao.setListeMessageEnvoyes(listeMessage);
				listeMessage = transformationListeNonPersistantBag(personneDao.getListeMessageRecus());
				personneDao.setListeMessageRecus(listeMessage);
			

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return personneDao;
	}

	private List<MessageDao> transformationListeNonPersistantBag(List<MessageDao> listeMessage) {
		List<MessageDao> listeMessageTrans = new ArrayList<MessageDao>();
		listeMessage.forEach(m -> {
			MessageDao message = new MessageDao(m.getId(), m.getObjet(), m.getMessage(), m.getDate(), m.getArchiveExp(),
					m.getArchiveDest(), m.getPersonneExp(), m.getPersonneDest());
			listeMessageTrans.add(message);

		});
		return listeMessageTrans;
	}

}
