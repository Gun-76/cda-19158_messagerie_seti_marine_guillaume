package fr.afpa.messagerie.dao.iservice;

import fr.afpa.messagerie.dao.entites.PersonneDao;

public interface IServicePersonneDao {
	
	boolean savePersonne(PersonneDao personneDao);
	 boolean updatePerson(PersonneDao personneDao);
	 boolean deletePersonne(PersonneDao personneDao) ;

}
