package fr.afpa.messagerie.dao.entites;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"id","objet"})

@Entity
@Table(name = "message")
@NamedQuery(name = "getMessageId", query = "from MessageDao where id=:id")
public class MessageDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "message_generator")
	@SequenceGenerator(name = "message_generator", sequenceName = "seq_message",allocationSize = 1)
	@Column (name="id_message", updatable = false, nullable = false )
	int id;
	
	@Column (name="objet", updatable = true, nullable = false,length = 100)
	String objet;
	
	@Column (name="message", updatable = true, nullable = false,length = 100)
	String message;
	
	@Column (name="date", updatable = true, nullable = false,length = 100)
	LocalDateTime date;
	
	@Column (name="archiver_exp", updatable = true, nullable = false)
	Boolean archiveExp;
	
	@Column (name="archiver_dest", updatable = true, nullable = false)
	Boolean archiveDest;
	
	@ManyToOne(
			cascade= {CascadeType.PERSIST }
			)
	@JoinColumn(name="id_exp")
	PersonneDao personneExp;

	@ManyToOne(
			cascade= {CascadeType.PERSIST }
			)
	@JoinColumn(name="id_dest")
	PersonneDao personneDest;
}
