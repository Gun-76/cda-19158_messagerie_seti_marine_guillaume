package fr.afpa.messagerie.dao.persistance;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.MessageDao;
import fr.afpa.messagerie.dao.iservice.IServiceMessageDao;
import fr.afpa.messagerie.utils.HibernateUtils;

@Component
public class ServiceMessageDao implements IServiceMessageDao {

	@Override
	public boolean saveMessage(MessageDao messageDao) {

		Session s = null;
		Transaction tx = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.save(messageDao);
			tx.commit();
			retour = true;
		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}

	@Override
	public MessageDao getMessageId(String id) {
		Session s = null;
		MessageDao messageDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;

			q = s.getNamedQuery("getMessageId");
			q.setParameter("id", Integer.parseInt(id));

			
				messageDao = (MessageDao) q.getSingleResult();


		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return messageDao;
	}

	@Override
	public boolean updateMessage(MessageDao messageDao) {
		Session s = null;
		Transaction tx = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.update(messageDao);
			tx.commit();
			retour = true;
		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}
}
