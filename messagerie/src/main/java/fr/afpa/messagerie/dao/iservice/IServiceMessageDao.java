package fr.afpa.messagerie.dao.iservice;

import fr.afpa.messagerie.dao.entites.MessageDao;
import fr.afpa.messagerie.metier.entites.Message;

public interface IServiceMessageDao {

	/**
	 * service dao de sauvegarde d'un message
	 * @param messageDao le message � sauvegarder
	 * @return true si la sauvegarde a �t� effectu�
	 */
	public boolean saveMessage(MessageDao messageDao);

	/**
	 * service de r�cup�ration d'un message via l'id
	 * @param id : l'id du message � selectionner
	 * @return le messageDao correspondant � l'id
	 */
	public MessageDao getMessageId(String id);

	/**
	 * service Dao d'update d'un message
	 * @param messageDao le messageDao � modifier
	 * @return true si la modification a �t� effectu�
	 */
	boolean updateMessage(MessageDao messageDao);

}
