package fr.afpa.messagerie.dao.iservice;

import java.util.List;

import fr.afpa.messagerie.dao.entites.PersonneDao;

public interface IServiceRechercheDao {



	/**
	 * service dao qui execute la requete pour avvoir toutes les personneDAO
	 * 
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public List<PersonneDao> getAllPersonnes();

	/**
	 * service dao qui execute la requete de recherche personneDAO via le
	 * login
	 * @param login        : le login 
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public PersonneDao rechercheLogin( String valeur);

}
