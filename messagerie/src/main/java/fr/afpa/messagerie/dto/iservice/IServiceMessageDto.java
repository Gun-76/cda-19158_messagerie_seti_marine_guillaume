package fr.afpa.messagerie.dto.iservice;

import fr.afpa.messagerie.dao.entites.MessageDao;
import fr.afpa.messagerie.metier.entites.Message;

public interface IServiceMessageDto {

	/**
	 * service Dto d'envoi d'un message, transforme l'entite metier en entite persistante et l'envoi au service DAO pour la sauvegarde
	 * @param message le message a sauvegarder (envoyer)
	 * @return true si le message a �t� sauvegard�
	 */
	boolean envoiMessage(Message message);
	
	/**
	 * service DTO de r�cup�ration d'un message via son id
	 * @param id : l'id du message � r�cup�rer
	 * @return le message transforme en entite metier
	 */
	Message getMessageId(String id);
	
	/**
	 * service dto d'update d'un message
	 * @param message le message � modifier
	 * @return true si les modifications ont �t� enregistr�
	 */
	boolean updateMessage(Message message);
	/**
	 * methode qui permet de transformer une entit� message metier en entit�
	 * messageDao
	 * 
	 * @param message : entité fonction metier � transformer
	 * @return : une entit� messageDao correspondante � l'entite message metier
	 */
	public  MessageDao messageMetierToMessageDao(Message message);
	
	/**
	 * methode qui permet de transformer une entit� MessageDao en entit� message
	 * metier
	 * 
	 * @param messageDao : entit� MessageDao � transformer
	 * @return : une entit� message metier correspondante � l'entit� MessageDao
	 */
	public  Message messageDaoToMessageMetier(MessageDao messageDao);

}
