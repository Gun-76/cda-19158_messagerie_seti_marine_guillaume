package fr.afpa.messagerie.dto.iservice;

import java.util.List;

import fr.afpa.messagerie.metier.entites.Personne;

public interface IServiceRechercheDto {


	/**
	 * service DTO qui sert de lien entre le service metier de recuperation de
	 * toutes les personnes et le service dao, transforme la liste d'entité
	 * PersonneDao renvoyé par le DAO en liste d'entité personne metier avant de
	 * la renvoyer au service metier
	 * 
	 * @return une liste d'entités personnes metiers
	 */
	public List<Personne> getAllPersonnes();

	/**
	 * service DTO qui sert de lien entre le service metier de recherche personne
	 * via login et le service dao(effectue la recheche demandée), transforme la
	 * PersonneDao renvoyé par le DAO en personne metier avant de la renvoyer au
	 * service metier
	 * 
	 * @param login : le login
	 * @return une personne metier, correspondant à la recherche
	 */
	public Personne rechercheLogin(String login);

}
