package fr.afpa.messagerie.dto.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.MessageDao;
import fr.afpa.messagerie.dao.iservice.IServiceMessageDao;
import fr.afpa.messagerie.dto.iservice.IServiceAuthentificationDto;
import fr.afpa.messagerie.dto.iservice.IServiceMessageDto;
import fr.afpa.messagerie.dto.iservice.IServicePersonneDto;
import fr.afpa.messagerie.dto.iservice.IServiceRoleDto;
import fr.afpa.messagerie.metier.entites.Message;
import fr.afpa.messagerie.metier.entites.Personne;

@Component
public class ServiceMessageDto implements IServiceMessageDto {

	@Override
	public boolean envoiMessage(Message message) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDao iServMesDao = (IServiceMessageDao) context.getBean("serviceMessageDao");
		return iServMesDao.saveMessage(messageMetierToMessageDao(message));
	}
	
	@Override
	public Message getMessageId(String id) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDao iServMesDao = (IServiceMessageDao) context.getBean("serviceMessageDao");
		return messageDaoToMessageMetier(iServMesDao.getMessageId(id) );
	}

	@Override
	public boolean updateMessage(Message message) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDao iServMesDao = (IServiceMessageDao) context.getBean("serviceMessageDao");
		return iServMesDao.updateMessage(messageMetierToMessageDao(message));
	}

	@Override
	public MessageDao messageMetierToMessageDao(Message message) {
		if(message!=null) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersDto = (IServicePersonneDto) context.getBean("servicePersonneDto");

		MessageDao messageDao = new MessageDao(message.getId(), message.getObjet(), message.getMessage(),
				message.getDate(), message.getArchiveExp(), message.getArchiveDest(),
				iServPersDto.personneMetierToPersonneDao(message.getPersonneExp()),
				iServPersDto.personneMetierToPersonneDao(message.getPersonneDest()));

		return messageDao;
		}
		return null;
	}

	
	@Override
	public Message messageDaoToMessageMetier(MessageDao messageDao) {
		if(messageDao!=null) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersDto = (IServicePersonneDto) context.getBean("servicePersonneDto");
		IServiceAuthentificationDto iServAuthDto = (IServiceAuthentificationDto) context.getBean("serviceAuthentificationDto");
		IServiceRoleDto iServRoleDto = (ServiceRoleDto) context.getBean("serviceRoleDto");


		
		Personne personneExp = new Personne(messageDao.getPersonneExp().getId(), messageDao.getPersonneExp().getNom(),
				messageDao.getPersonneExp().getPrenom(), messageDao.getPersonneExp().getMail(),
				messageDao.getPersonneExp().getTel(), messageDao.getPersonneExp().getAdresse(),
				messageDao.getPersonneExp().getDateDeNaissance(), iServRoleDto.roleDaoToRoleMetier(messageDao.getPersonneExp().getRole()), null, null, iServAuthDto.authDaoToAuthMetier(messageDao.getPersonneExp().getAuthentification()),
				messageDao.getPersonneExp().isActif());

		Personne personneDest = new Personne(messageDao.getPersonneDest().getId(),
				messageDao.getPersonneDest().getNom(), messageDao.getPersonneDest().getPrenom(),
				messageDao.getPersonneDest().getMail(), messageDao.getPersonneDest().getTel(),
				messageDao.getPersonneDest().getAdresse(), messageDao.getPersonneDest().getDateDeNaissance(), iServRoleDto.roleDaoToRoleMetier(messageDao.getPersonneDest().getRole()),
				null, null, iServAuthDto.authDaoToAuthMetier(messageDao.getPersonneDest().getAuthentification()), messageDao.getPersonneDest().isActif());

		Message message = new Message(messageDao.getId(), messageDao.getObjet(), messageDao.getMessage(),
				messageDao.getDate(), messageDao.getArchiveExp(), messageDao.getArchiveDest(), personneExp,
				personneDest);

		return message;
		}
		return null;
	}

	

}
