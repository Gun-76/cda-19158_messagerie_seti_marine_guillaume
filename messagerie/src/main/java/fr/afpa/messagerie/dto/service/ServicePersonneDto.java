package fr.afpa.messagerie.dto.service;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.collection.internal.PersistentBag;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.PersonneDao;
import fr.afpa.messagerie.dao.iservice.IServicePersonneDao;
import fr.afpa.messagerie.dao.persistance.ServicePersonneDao;
import fr.afpa.messagerie.dto.iservice.IServiceAuthentificationDto;
import fr.afpa.messagerie.dto.iservice.IServiceMessageDto;
import fr.afpa.messagerie.dto.iservice.IServicePersonneDto;
import fr.afpa.messagerie.dto.iservice.IServiceRoleDto;
import fr.afpa.messagerie.metier.entites.Message;
import fr.afpa.messagerie.metier.entites.Personne;

@Component
public class ServicePersonneDto implements IServicePersonneDto {
	
	@Override
	public boolean savePersonne(Personne personne) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		IServicePersonneDao iServPersDao = (IServicePersonneDao) context
				.getBean("servicePersonneDao");
		
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		return iServPersDao.savePersonne(personneDao);

	}

	@Override
	public boolean deletePersonne(Personne personne) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		IServicePersonneDao iServPersDao = (IServicePersonneDao) context
				.getBean("servicePersonneDao");
		return iServPersDao.deletePersonne(personneMetierToPersonneDao(personne));
	}


	@Override
	public boolean updatePersonne(Personne personne) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		IServicePersonneDao iServPersDao = (IServicePersonneDao) context
				.getBean("servicePersonneDao");
		return iServPersDao.updatePerson(personneMetierToPersonneDao(personne));
	}

	@Override
	public PersonneDao personneMetierToPersonneDao(Personne personne) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		IServiceAuthentificationDto iServAuthDto = (IServiceAuthentificationDto) context
				.getBean("serviceAuthentificationDto");
		IServiceRoleDto iServRoleDto = (IServiceRoleDto) context.getBean("serviceRoleDto");

		PersonneDao personneDao = new PersonneDao();
		personneDao.setId(personne.getId());
		personneDao.setNom(personne.getNom());
		personneDao.setActif(personne.isActif());
		personneDao.setAdresse(personne.getAdresse());
		personneDao.setAuthentification(iServAuthDto.authMetierToAuthDao(personne.getAuthentification()));
		personneDao.setDateDeNaissance(personne.getDateDeNaissance());
		// personneDao.setFonction(ServiceFonctionDto.fonctionMetierToFonctionDao(personne.getFonction()));
		personneDao.setMail(personne.getMail());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setRole(iServRoleDto.roleMetierToRoleDao(personne.getRole()));
		personneDao.setTel(personne.getTel());
		personneDao.setActif(personne.isActif());

		return personneDao;
	}

	@Override
	public Personne personneDaoToPersonneMetier(PersonneDao personneDao) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDto iServMesDto = (IServiceMessageDto) context.getBean("serviceMessageDto");

		IServiceAuthentificationDto iServAuthDto = (IServiceAuthentificationDto) context
				.getBean("serviceAuthentificationDto");
		IServiceRoleDto iServRoleDto = (IServiceRoleDto) context.getBean("serviceRoleDto");

		Personne personne = new Personne();
		personne.setId(personneDao.getId());
		personne.setNom(personneDao.getNom());
		personne.setActif(personneDao.isActif());
		personne.setAdresse(personneDao.getAdresse());
		personne.setAuthentification(iServAuthDto.authDaoToAuthMetier(personneDao.getAuthentification()));
		personne.setDateDeNaissance(personneDao.getDateDeNaissance());
		personne.setMail(personneDao.getMail());
		personne.setPrenom(personneDao.getPrenom());
		personne.setRole(iServRoleDto.roleDaoToRoleMetier(personneDao.getRole()));
		personne.setTel(personneDao.getTel());

		if (!(personneDao.getListeMessageRecus() instanceof PersistentBag)) {
			List<Message> listeMessage = personneDao.getListeMessageEnvoyes().stream()
					.map(iServMesDto::messageDaoToMessageMetier).collect(Collectors.toList());
			personne.setMessageEnvoyees(listeMessage);
			listeMessage = personneDao.getListeMessageRecus().stream().map(iServMesDto::messageDaoToMessageMetier)
					.collect(Collectors.toList());
			personne.setMessageRecus(listeMessage);
		}
		return personne;
	}

	

}
