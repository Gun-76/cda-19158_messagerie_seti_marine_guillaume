package fr.afpa.messagerie.dto.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.PersonneDao;
import fr.afpa.messagerie.dao.iservice.IServiceRechercheDao;
import fr.afpa.messagerie.dto.iservice.IServicePersonneDto;
import fr.afpa.messagerie.dto.iservice.IServiceRechercheDto;
import fr.afpa.messagerie.metier.entites.Personne;

@Component
public class ServiceRechercheDto implements IServiceRechercheDto {



	@Override
	public List<Personne> getAllPersonnes() {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersDto = (IServicePersonneDto) context.getBean("servicePersonneDto");
		IServiceRechercheDao iServRechDao = (IServiceRechercheDao) context.getBean("serviceRechercheDao");

		List<PersonneDao> listePersDao = iServRechDao.getAllPersonnes();
		List<Personne> listePers = null;
		if (listePersDao != null) {
			listePers = listePersDao.stream().map(iServPersDto::personneDaoToPersonneMetier)
					.collect(Collectors.toList());
		}
		return listePers;
	}

	@Override
	public Personne rechercheLogin(String login) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersDto = (IServicePersonneDto) context.getBean("servicePersonneDto");
		IServiceRechercheDao iServRechDao = (IServiceRechercheDao) context.getBean("serviceRechercheDao");

		PersonneDao personneDao = iServRechDao.rechercheLogin(login);

		if (personneDao != null) {
			return iServPersDto.personneDaoToPersonneMetier(personneDao);
		}
		return null;

	}

}
