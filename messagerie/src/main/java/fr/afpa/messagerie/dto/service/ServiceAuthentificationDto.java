package fr.afpa.messagerie.dto.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.AuthentificationDao;
import fr.afpa.messagerie.dao.persistance.ServiceAuthentificationDao;
import fr.afpa.messagerie.dto.iservice.IServiceAuthentificationDto;
import fr.afpa.messagerie.dto.iservice.IServicePersonneDto;
import fr.afpa.messagerie.metier.entites.Authentification;
import fr.afpa.messagerie.metier.entites.Personne;

@Component
public class ServiceAuthentificationDto implements IServiceAuthentificationDto {

	@Override
	public Personne authentification(Authentification auth) {
		Personne personneAuth = null;
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersDto = (IServicePersonneDto) context.getBean("servicePersonneDto");

		AuthentificationDao authDaoVerif = authMetierToAuthDao(auth);
		ServiceAuthentificationDao servAuthDao = (ServiceAuthentificationDao) context
				.getBean("serviceAuthentificationDao");
		AuthentificationDao authDaoRecup = servAuthDao.authentification(authDaoVerif);
		if (authDaoRecup != null) {
			personneAuth = iServPersDto.personneDaoToPersonneMetier(authDaoRecup.getPersonne());
		}
		return personneAuth;
	}

	@Override
	public AuthentificationDao authMetierToAuthDao(Authentification auth) {
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		return authDao;
	}

	@Override
	public Authentification authDaoToAuthMetier(AuthentificationDao authDao) {
		Authentification auth = new Authentification(authDao.getLogin(), authDao.getMdp());
		return auth;
	}

}
