package fr.afpa.messagerie.dto.iservice;

import fr.afpa.messagerie.dao.entites.AuthentificationDao;
import fr.afpa.messagerie.metier.entites.Authentification;
import fr.afpa.messagerie.metier.entites.Personne;

public interface IServiceAuthentificationDto {

	/**
	 * service authentification Dto, transforme l'entité Authentification en
	 * parametre en entité AuthentificationDAO, ensuite appel le service DAO
	 * authentification pour vérifier que l'authentification est correcte si le
	 * service DAO renvoie une AuthentificationDAO, il transforme la personne
	 * correspondant à l'authentification en entité personne et la renvoie
	 * 
	 * @param auth : authentification metier à controler
	 * @return une entité personne metier si l'authentification est correcte, null
	 *         sinon
	 */
	public Personne authentification(Authentification auth);

	/**
	 * methode qui permet de transformer une entité authentification métier en
	 * entité authentificationDao
	 * 
	 * @param auth : entité authentification métier à transformer
	 * @return : une entité authentificationDao correspondante à l'entité
	 *         authentification métier
	 */
	public AuthentificationDao authMetierToAuthDao(Authentification auth);

	/**
	 * méthode qui permet de transformer une entité authentificationDao en entité
	 * authentification métier
	 * 
	 * @param auth : entité authentificationDao à transformer
	 * @return : une entité authentification métier correspondante à l'entité
	 *         authentificationDao
	 */
	public Authentification authDaoToAuthMetier(AuthentificationDao authDao);
}
