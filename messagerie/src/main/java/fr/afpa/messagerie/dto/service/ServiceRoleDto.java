package fr.afpa.messagerie.dto.service;

import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dao.entites.RoleDao;
import fr.afpa.messagerie.dto.iservice.IServiceRoleDto;
import fr.afpa.messagerie.metier.entites.Role;


@Component
public class ServiceRoleDto implements IServiceRoleDto {

	
	
	
	
	
	@Override
	public  RoleDao roleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setId(role.getId());
		roleDao.setLibelle(role.getLibelle());
		
		return roleDao;
	}
	
	
	
	@Override
	public  Role roleDaoToRoleMetier(RoleDao roleDao) {
		return new Role(roleDao.getId(), roleDao.getLibelle());
		
	}

}
