package fr.afpa.messagerie.dto.iservice;

import fr.afpa.messagerie.dao.entites.RoleDao;
import fr.afpa.messagerie.metier.entites.Role;

public interface IServiceRoleDto {

	/**
	 * méthode qui permet de transformer une entité role metier en entité roleDao
	 * 
	 * @param role : entité role metier à transformer
	 * @return : une entité roleDao correspondante à l'entite role metier
	 */
	public RoleDao roleMetierToRoleDao(Role role);

	/**
	 * méthode qui permet de transformer une entité roleDao en entité role metier
	 * 
	 * @param auth : entité roleDao à transformer
	 * @return : une entité role metier correspondante à l'entité roleDao
	 */
	public Role roleDaoToRoleMetier(RoleDao roleDao);
}
