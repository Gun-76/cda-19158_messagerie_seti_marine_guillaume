package fr.afpa.messagerie.dto.iservice;

import fr.afpa.messagerie.dao.entites.PersonneDao;
import fr.afpa.messagerie.metier.entites.Personne;

public interface IServicePersonneDto {

	/**
	 * Lien entre le service de création d'une personne et le service dao qui va
	 * créé la personne Dao dans la base de données
	 * 
	 * @param personne
	 * @return
	 */
	public boolean savePersonne(Personne personne);

	/**
	 * lien entre le service de suppression d'une personne et le service Dao qui
	 * supprime la personne
	 * 
	 * @param personne : une entite metier personne
	 * @return : true si la suppresion à été effectué, false sinon
	 */
	public boolean deletePersonne(Personne personne);

	/**
	 * Lien entre le service de modification d'une personne et le service dao qui va
	 * mettre à jour la personne Dao dans la base de données
	 * 
	 * @param personne : l'entite metier personne initialisé auparavant
	 * @return : true si l'update s'est à été effectué, false sinon
	 */
	public boolean updatePersonne(Personne personne);

	/**
	 * méthode qui permet de transformer une entité personne metier en entité
	 * personneDao
	 * 
	 * @param personne : entité personne metier à transformer
	 * @return : une entité personneDao correspondante à l'entite personne metier
	 */
	public PersonneDao personneMetierToPersonneDao(Personne personne);

	/**
	 * méthode qui permet de transformer une entité personneDao en entité
	 * personne metier
	 * 
	 * @param personneDao : entité personneDao à transformer
	 * @return : une entité personne métier correspondante à l'entité
	 *         personneDao
	 */
	public Personne personneDaoToPersonneMetier(PersonneDao personneDao);
}
