package fr.afpa.messagerie.metier.iservice;

import fr.afpa.messagerie.metier.entites.Personne;

public interface IServiceAuthentification {

	/**
	 * Service de controle de l'authentification, envoie au dto une entite
	 * authentification metier, si l'authentification est correcte, le dto renvoie
	 * la personne authentifi�e
	 * 
	 * @param login : le login � tester
	 * @param mdp : le mot de passe � tester
	 * @return une entite metier personne si l'authentification est correcte, null sinon
	 */
	Personne authentification(String login, String mdp);

}
