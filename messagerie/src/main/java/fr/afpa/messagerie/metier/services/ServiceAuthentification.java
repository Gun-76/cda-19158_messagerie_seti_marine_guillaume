package fr.afpa.messagerie.metier.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dto.iservice.IServiceAuthentificationDto;
import fr.afpa.messagerie.metier.entites.Authentification;
import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.iservice.IServiceAuthentification;


@Component
public class ServiceAuthentification implements IServiceAuthentification{

	
	@Override
	public Personne authentification(String login, String mdp) {
		Authentification auth = new Authentification(login, mdp);
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceAuthentificationDto  iServAuthDto= (IServiceAuthentificationDto) context.getBean("serviceAuthentificationDto");
	
		return iServAuthDto.authentification(auth);
	}

}
