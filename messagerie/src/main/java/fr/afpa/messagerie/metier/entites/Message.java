package fr.afpa.messagerie.metier.entites;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"id","objet"})
@EqualsAndHashCode
public class Message {

	int id;
	String objet;
	String message;
	LocalDateTime date;
	Boolean archiveExp;
	Boolean archiveDest;
	
	Personne personneExp;
	
	Personne personneDest;
	
	
	
}
