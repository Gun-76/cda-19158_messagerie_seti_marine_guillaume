package fr.afpa.messagerie.metier.iservice;

import java.time.LocalDate;

public interface IServicePersonne  {
	

	boolean creationPersonne(String nom, String prenom, String mail, String tel, String adresse,
			String dateDeNaissance, String login, String mdp, boolean actif);
		
		
	

	 boolean modificationPersonne(String nom, String prenom, String mail, String tel, String adresse,
				String dateDeNaissance,String login, String mdp,boolean actif );
	



}
