package fr.afpa.messagerie.metier.services;

import java.time.LocalDateTime;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dto.iservice.IServiceRechercheDto;
import fr.afpa.messagerie.dto.iservice.IServiceMessageDto;
import fr.afpa.messagerie.metier.entites.Message;
import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.iservice.IServiceMessage;

@Component
public class ServiceMessage implements IServiceMessage {

	@Override
	public String envoiMessage(String[] destinataires, String objet, String corps, String loginPersonneExp) {
		StringBuilder resultat = new StringBuilder();
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDto iServMesDto = (IServiceMessageDto) context.getBean("serviceMessageDto");
		IServiceRechercheDto iServRechDto = (IServiceRechercheDto) context.getBean("serviceRechercheDto");
		Personne persDest = null;
		boolean envoi;
		Message message = new Message(0, objet, corps, LocalDateTime.now(), false, false, null, null);

		// recuperation de la personne qui envoie le message
		Personne persExp = iServRechDto.rechercheLogin(loginPersonneExp);
		message.setPersonneExp(persExp);

		for (int i = 0; i < destinataires.length; i++) {

			// recuperation de la personne qui recoit le message
			persDest = iServRechDto.rechercheLogin(destinataires[i]);
			if (persDest != null) {
				message.setPersonneDest(persDest);
				envoi = iServMesDto.envoiMessage(message);
				if (envoi) {
					resultat.append(destinataires[i] + " : message envoy� \n");
				} else {
					resultat.append(destinataires[i] + " : erreur dans l'envoi du message \n");
				}
			} else {
				resultat.append(destinataires[i] + " : erreur destinataire inconnu \n");
			}

		}

		return resultat.toString();
	}

	@Override
	public boolean archiverMessage(int idPers, String id) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDto iServMesDto = (IServiceMessageDto) context.getBean("serviceMessageDto");

		// recuperation du message � archiver
		Message message = iServMesDto.getMessageId(id);

		// passage du boolean archive � true pour la personne souhaitant archiver le
		// message
		if (message.getPersonneExp().getId() == idPers) {
			message.setArchiveExp(true);
		}
		if (message.getPersonneDest().getId() == idPers) {
			message.setArchiveDest(true);
		}
		return iServMesDto.updateMessage(message);

	}

	@Override
	public Message getMessageId(String id) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessageDto iServMesDto = (IServiceMessageDto) context.getBean("serviceMessageDto");
		return  iServMesDto.getMessageId(id);
	}

}
