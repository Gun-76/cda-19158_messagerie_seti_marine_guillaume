package fr.afpa.messagerie.metier.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dto.iservice.IServiceRechercheDto;
import fr.afpa.messagerie.dto.service.ServiceRechercheDto;
import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.iservice.IServiceRecherche;

@Component
public class ServiceRecherche implements IServiceRecherche{
	
	


	
	@Override
	public List<Personne> getAllPersonnes() {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceRechercheDto iServRechDto = (IServiceRechercheDto) context.getBean("serviceRechercheDto");
		
		List<Personne> liste = new ArrayList<Personne>();
		liste = iServRechDto.getAllPersonnes();
		return liste;
	}
	
	@Override
	public Personne rechercheLogin(String login) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceRechercheDto iServRechDto = (IServiceRechercheDto) context.getBean("serviceRechercheDto");
		return  iServRechDto.rechercheLogin(login);
	}

}
