package fr.afpa.messagerie.metier.iservice;

import fr.afpa.messagerie.metier.entites.Message;

public interface IServiceMessage {
	
	/**
	 * service metier de r�cupearion d'un message via l'id
	 * @param id : l'id du message
	 * @return : une entite metier correspondante � l'id du message
	 */
	Message getMessageId(String id);

	/**
	 * Service m�tier d'envoi des messages
	 * 
	 * @param destinataires    : un tableau correspondant au login des destinataires
	 * @param objet            : l'objet du message
	 * @param corps            : le corps du message
	 * @param loginPersonneExp : le login de la personne qui envoie le message
	 * @return : une chaine de caracteres correspondant au resultat de l'envoi
	 */
	public String envoiMessage(String[] destinataires, String objet, String corps, String loginPersonneExp);

	/**
	 * Service metier d'archivage d'un mail
	 * 
	 * @param idPersonne l'id de la personne qui souhaite archiver le message
	 * @param idMessage  : id du message a archiver
	 * @return true si le message est archiv�
	 */
	boolean archiverMessage(int idPersonne, String idMessage);
}
