package fr.afpa.messagerie.metier.iservice;

import java.util.List;

import fr.afpa.messagerie.metier.entites.Personne;

public interface IServiceRecherche {
	
	
	
	/**
	 * Service qui renvoie la liste de tous les personnnes
	 * @return une liste d'entité métier personne,  correspondant à tous les personnes
	 */
	public List<Personne> getAllPersonnes();
	
	/**
	 * Service de recherche de personne via le login 
	 * @param valeur :  le login
	 * @return une  d'entité métier personne,  correspondant à la recherche
	 */
	public Personne rechercheLogin(String login);
}
