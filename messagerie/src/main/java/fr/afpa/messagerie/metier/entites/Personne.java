package fr.afpa.messagerie.metier.entites;

import java.time.LocalDate;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"id","nom","prenom","mail","tel","adresse","dateDeNaissance","role","authentification"})

public class Personne {

	int id;
	String nom;
	String prenom;
	String mail;
	String tel;
	String adresse;
	LocalDate dateDeNaissance;
	Role role;
	List <Message> messageEnvoyees;
	List <Message> messageRecus;
	
	Authentification authentification;
	boolean actif;
}
