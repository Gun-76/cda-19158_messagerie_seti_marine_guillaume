package fr.afpa.messagerie.metier.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.afpa.messagerie.dto.iservice.IServicePersonneDto;
import fr.afpa.messagerie.dto.service.ServicePersonneDto;
import fr.afpa.messagerie.metier.entites.Authentification;
import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.entites.Role;
import fr.afpa.messagerie.metier.iservice.IServicePersonne;
import fr.afpa.messagerie.metier.iservice.IServiceRecherche;

@Component
public class ServicePersonne implements IServicePersonne {

	/**
	 * Service de suppression d'une personne
	 * 
	 * @param login : le login de la personne à supprimer
	 * @return : true si la suppresion à été effectué, false sinon
	 */

	public boolean deletePersonne(String login) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersonneDto = (IServicePersonneDto) context.getBean("servicePersonneDto");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

		Personne personne = iServRech.rechercheLogin(login);
		if (personne.getRole().getId() != 1) {
			return iServPersonneDto.deletePersonne(personne);
		}
		return false;
	}

	/**
	 * Service pour le update de la personne
	 * 
	 * @param nom      : nom modifié ou non
	 * @param prenom   : prenom ou non
	 * @param mail     : mail ou non
	 * @param tel      : tel ou non
	 * @param adresse  : adresse ou non
	 * @param date     : date ou non
	 * @param actif    : actif ou non
	 * @param login    : login ou non
	 * @param role     : role ou non
	 * @param fonction
	 * @return un boolean true si la mise à jour à bien ete effectué, false dans
	 *         le cas contraire
	 */
	public boolean modificationPersonne(String nom, String prenom, String mail, String tel, String adresse,
			String dateDeNaissance, String login, String mdp, boolean actif) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersonneDto = (IServicePersonneDto) context.getBean("servicePersonneDto");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

		Personne personne = iServRech.rechercheLogin(login);

		LocalDate date2 = LocalDate.parse(dateDeNaissance, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setMail(mail);
		personne.setTel(tel);
		personne.setAdresse(adresse);
		personne.setDateDeNaissance(date2);
		personne.getAuthentification().setLogin(login);
		personne.getAuthentification().setMdp(mdp);
		personne.setActif(actif);
		return iServPersonneDto.updatePersonne(personne);

	}

	/**
	 * Service de creation d'une personne
	 * 
	 * @param nom             : le nom de la personne
	 * @param prenom          : le prenom de la personne
	 * @param mail            : le mail de la personne
	 * @param tel             : le téléphone de la personne
	 * @param adresse         : l'adresse de la personne
	 * @param dateDeNaissance : la date de naissance de la personne
	 * @param idRole          : le role de la personne
	 * @param idFonction      : la fonction de la personne
	 * @param login           : le login de la personne
	 * @param mdp             : le mot de passe de la personne
	 * @param actif           :
	 * @return : true si la personne est bien crée, false sinon
	 */

	public boolean creationPersonne(String nom, String prenom, String mail, String tel, String adresse,
			String dateDeNaissance, String login, String mdp, boolean actif) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonneDto iServPersonneDto = (IServicePersonneDto) context.getBean("servicePersonneDto");

		Role role = new Role();
		role.setId(2);

		Authentification authentification = new Authentification();
		authentification.setLogin(login);
		authentification.setMdp(mdp);
		Personne personne = new Personne();
		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setMail(mail);
		personne.setTel(tel);
		personne.setAdresse(adresse);
		personne.setDateDeNaissance(LocalDate.parse(dateDeNaissance));
		personne.setRole(role);
		personne.setAuthentification(authentification);
		personne.setActif(actif);

		return iServPersonneDto.savePersonne(personne);

	}

}
