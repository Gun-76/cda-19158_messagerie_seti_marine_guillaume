package fr.afpa.messagerie.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.messagerie.constantes.Constantes;
import fr.afpa.messagerie.controles.ControleSaisie;
import fr.afpa.messagerie.metier.entites.Message;
import fr.afpa.messagerie.metier.entites.Personne;
import fr.afpa.messagerie.metier.iservice.IServiceAuthentification;
import fr.afpa.messagerie.metier.iservice.IServiceMessage;
import fr.afpa.messagerie.metier.iservice.IServicePersonne;
import fr.afpa.messagerie.metier.iservice.IServiceRecherche;
import fr.afpa.messagerie.metier.services.ServicePersonne;
import fr.afpa.messagerie.metier.services.ServiceRecherche;

@Controller
@SessionAttributes("persAuth")
public class HomeController {

	@ModelAttribute("persAuth")
	public Personne persAuth() {
		return new Personne();
	}

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Controller methode get de la page authentification
	 * 
	 * @param mv : le modelAndView
	 * @return le modelAndView remplit de la page authentification
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView authentificationGet(ModelAndView mv) {
		mv.setViewName("authentification");
		return mv;
	}

	/**
	 * ontroller methode post de la page authentification, effectue la vérification
	 * de l'authentification et dispatche selon le resultat
	 * 
	 * @param mv    :le modelAndView
	 * @param login : le login entré
	 * @param mdp   : le mot de passe entré
	 * @return le modelAndView remplit selon le resultat de l'authentification
	 *         (boite de reception si ok, page authentification si ko)
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST, params = { "login", "mdp" })
	public ModelAndView authentificationPost(ModelAndView mv, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {
		String uri;

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceAuthentification iServAuth = (IServiceAuthentification) context.getBean("serviceAuthentification");

		uri = "authentification";

		// recuperation de la personne authentifier, null si l'authenfication a
		// �chou�

		Personne persAuth;
		persAuth = iServAuth.authentification(login, mdp);
		Personne p = iServAuth.authentification(login, mdp);
		if (persAuth != null) {
			if (persAuth.isActif()) {
				uri = "/messagerie/boitereception";
				mv.addObject("persAuth", persAuth);
				RedirectView redirectView = new RedirectView(uri);
				mv.setView(redirectView);
			} else {
				mv.addObject("KO", "Contacter l'administrateur pour activer votre compte");
				mv.setViewName(uri);
			}
		} else {
			mv.addObject("KO", "Erreur login ou mot de passe incorrects ");
			mv.setViewName(uri);
		}

		return mv;
	}

	/**
	 * c'est ic que je dois modifier en cas de probleme avec la recuperation de
	 * donn�es du formulaire
	 * 
	 * @param mv
	 * @return
	 */

	@RequestMapping(value = "/creation", method = RequestMethod.GET)
	public ModelAndView creationUtilisateurGet(ModelAndView mv) {

		mv.setViewName("creationUtilisateur");
		return mv;
	}

	@RequestMapping(value = "/creation", method = RequestMethod.POST, params = { "nom", "prenom", "email", "tel",
			"adresse", "date_naissance", "login", "mdp" })
	public ModelAndView creationUtilisateurPost(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "email") String email,
			@RequestParam(value = "date_naissance") String dateNaissance, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "adresse") String adresse, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServicePersonne iServPers = (IServicePersonne) context.getBean("servicePersonne");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");
		
		mv.addObject("login",login);
		mv.addObject("mail",email);
		mv.addObject("prenom",prenom);
		mv.addObject("nom",nom);
		mv.addObject("tel",tel);
		mv.addObject("adresse",adresse);
		mv.addObject("dateNaissance",dateNaissance);
		mv.addObject("mdp",mdp);
		
		
		
		boolean ok = true;
		if (!ControleSaisie.isLogin(login) || iServRech.rechercheLogin(login)!=null) {
			ok = false;
			mv.addObject("loginKO", "*login déjà existant ou non valide(pas de virgule et espace)");
			mv.addObject(login);
		}
		if (!ControleSaisie.isMail(email)) {
			ok = false;
			mv.addObject("mailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(tel) ){
			ok = false;
			mv.addObject("telKO", "*10 chiffres sans espace");
		}
		
		if (!ControleSaisie.isNonVide(mdp)) {
			ok = false;
			mv.addObject("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(adresse)) {
			ok = false;
			mv.addObject("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValide(dateNaissance)) {
			ok = false;
			mv.addObject("dateKO", "*date invalide");
		}
		if (!ControleSaisie.isNom(prenom)) {
			ok = false;
			mv.addObject("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(nom)) {
			ok = false;
			mv.addObject("nomKO", "*uniquement des lettres sans accent");
		}
		
		if (!ok) {
			mv.setViewName("creationUtilisateur");
		} else {
			iServPers.creationPersonne(nom, prenom, email, tel, adresse, dateNaissance, login, mdp, false);
			mv.addObject(iServPers);
			mv.setViewName("compteCree");
		}

		return mv;

	}

	@RequestMapping(value = "/modification", method = RequestMethod.GET)
	public ModelAndView modificationUtilisateurGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {

		if (persAuth.getId() != 0) {
			mv.setViewName("modificationUtilisateur");
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// on envoie l'objet personne au modelandview qui l'envoie � la jsp
			Personne personne = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());
			mv.addObject("personne", personne);
			
			
			

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	@RequestMapping(value = "/modification", method = RequestMethod.POST, params = { "nom", "prenom", "email", "tel",
			"adresse", "date_naissance", "mdp", "actif", "recuplogin" })
	public ModelAndView modificationUtilisateurPost(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "email") String email,
			@RequestParam(value = "date_naissance") String dateNaissance, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "adresse") String adresse, @RequestParam(value = "mdp") String mdp,
			@RequestParam(value = "actif") String actif, @RequestParam(value = "recuplogin") String login,
			@ModelAttribute("persAuth") Personne persAuth) {
		persAuth.setActif(true);
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");
		IServicePersonne iServPers = (IServicePersonne) context.getBean("servicePersonne");
		


		
		
		
		
		boolean ok = true;
		
		if (!ControleSaisie.isMail(email)) {
			ok = false;
			mv.addObject("emailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(tel) ){
			ok = false;
			mv.addObject("telKO", "*10 chiffres sans espace");
		}
		
		if (!ControleSaisie.isNonVide(mdp)) {
			ok = false;
			mv.addObject("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(adresse)) {
			ok = false;
			mv.addObject("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValide(dateNaissance)) {
			ok = false;
			mv.addObject("dateKO", "*date invalide");
		}
		if (!ControleSaisie.isNom(prenom)) {
			ok = false;
			mv.addObject("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(nom)) {
			ok = false;
			mv.addObject("nomKO", "*uniquement des lettres sans accent");
		}
		if (!ok) {
			mv.setViewName("modificationUtilisateur");
			Personne personne = iServRech.rechercheLogin(login);
			mv.addObject("personne", personne);
		}
		
		else {
			iServPers.modificationPersonne(nom, prenom, email, tel, adresse, dateNaissance, login, mdp,
					Boolean.parseBoolean(actif));
			RedirectView redirectView = new RedirectView("/messagerie/boitereception");
			mv.setView(redirectView);
		}
		
		
		
		return mv;

	}

	/**
	 * Controller methode get de la page nouveau message
	 * 
	 * @param mv       : le modelAndView
	 * @param persAuth : la personne authentifiée
	 * @return le ModelAndView remplit de la page nouveau message
	 */
	@RequestMapping(value = "/nouveaumessage", method = RequestMethod.GET)
	public ModelAndView nouveauMessageGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {

		if (persAuth.getId() != 0 && persAuth.isActif()) {
			mv.setViewName("nouveaumessage");
		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode post de la page nouveau message
	 * 
	 * @param mv           : le modelAndView
	 * @param destinataire : les destinataires séparé par une virgule
	 * @param objet        : l'objet du message
	 * @param message      : le message
	 * @param persAuth     : la personne authentifiée
	 * @return le ModelAndView remplit avec le resultat de l'envoi du message
	 */
	@RequestMapping(value = "/nouveaumessage", method = RequestMethod.POST)
	public ModelAndView nouveauMessagePost(ModelAndView mv, @RequestParam(value = "destinataire") String destinataire,
			@RequestParam(value = "objet") String objet, @RequestParam(value = "message") String message,
			@ModelAttribute("persAuth") Personne persAuth) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessage iServMes = (IServiceMessage) context.getBean("serviceMessage");

		String resultat = iServMes.envoiMessage(destinataire.replaceAll(" ", "").split(","), objet, message,
				persAuth.getAuthentification().getLogin());

		mv.addObject("resultat", resultat);
		mv.setViewName("resultatenvoimessage");

		return mv;
	}

	/**
	 * Controller methode get de la page boite de reception
	 * 
	 * @param mv       :le modelAndView
	 * @param persAuth : la personne authentifiée
	 * @return : la boite de reception contenant les n 1er messages(5)
	 */
	@RequestMapping(value = "/boitereception", method = RequestMethod.GET)
	public ModelAndView boiteReceptionGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			mv.setViewName("boitereception");

			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessage = pers.getMessageRecus();
			listeMessage = listeMessage.stream().filter(m -> !m.getArchiveDest()).collect(Collectors.toList());
			listeMessage = listeMessage.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = 0;
			int fin = Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessage.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessage.size()) {
				fin = listeMessage.size();
			}

			List<Message> listeMessageAfficher = listeMessage.subList(debut, fin);
			mv.addObject("listeMessage", listeMessageAfficher);
			mv.addObject("nbPage", nombrePage);

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode get de la page boite de reception
	 * 
	 * @param mv       :le modelAndView
	 * @param page     : le numero de page cliqué
	 * @param persAuth : la personne authentifiée
	 * @return : la boite de reception contenant les n messages(5) de la page
	 */
	@RequestMapping(value = "boitereception/page", method = RequestMethod.GET, params = { "page" })
	public ModelAndView boiteReceptionPage(ModelAndView mv, @RequestParam(value = "page") int page,
			@ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessage = pers.getMessageRecus();
			listeMessage = listeMessage.stream().filter(m -> !m.getArchiveDest()).collect(Collectors.toList());
			List<Message> listeMessageAfficher = null;
			listeMessage = listeMessage.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = page * Constantes.NB_LIGNE - Constantes.NB_LIGNE;
			int fin = page * Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessage.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessage.size()) {
				fin = listeMessage.size();
			}
			listeMessageAfficher = listeMessage.subList(debut, fin);

			mv.setViewName("boitereception");
			mv.addObject("nbPage", nombrePage);
			mv.addObject("listeMessage", listeMessageAfficher);
		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Controller methode get de la page admin
	 * 
	 * @param mv       :le modelAndView
	 * @param persAuth : la personne authentifiée
	 * @return : la liste des utilisateurs contenant les n 1er utilisateurs(5)
	 */
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView adminGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

		if (persAuth.getId() != 0 && persAuth.getRole().getId() == Constantes.ID_ROLE_ADMIN) {
			mv.setViewName("admin");
			List<Personne> listePersonne = iServRech.getAllPersonnes();
			int debut = 0;
			int fin = Constantes.NB_LIGNE;
			int nombreTotalPers = listePersonne.size();
			int nombrePage = (nombreTotalPers / Constantes.NB_LIGNE)
					+ (nombreTotalPers % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listePersonne.size()) {
				fin = listePersonne.size();
			}

			listePersonne = listePersonne.stream().sorted((p1, p2) -> p1.getId() - p2.getId())
					.collect(Collectors.toList());

			listePersonne = listePersonne.subList(debut, fin);

			mv.addObject("nbPage", nombrePage);
			mv.addObject("listePersonne", listePersonne);

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode get de la page boite de reception
	 * 
	 * @param mv       :le modelAndView
	 * @param page     : le numero de page cliqué
	 * @param persAuth : la personne authentifiée
	 * @return : la liste des utilisateurs contenant les n utilisateurs(5) de la
	 *         page
	 */
	@RequestMapping(value = "admin/page", method = RequestMethod.GET, params = { "page" })
	public ModelAndView adminPage(ModelAndView mv, @RequestParam(value = "page") int page,
			@ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.getRole().getId() == Constantes.ID_ROLE_ADMIN) {
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			List<Personne> listePersonne = iServRech.getAllPersonnes();
			listePersonne = listePersonne.stream().sorted((p1, p2) -> p1.getId() - p2.getId())
					.collect(Collectors.toList());

			int debut = page * Constantes.NB_LIGNE - Constantes.NB_LIGNE;
			int fin = page * Constantes.NB_LIGNE;
			int nombreTotalPersonne = listePersonne.size();
			int nombrePage = (nombreTotalPersonne / Constantes.NB_LIGNE)
					+ (nombreTotalPersonne % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listePersonne.size()) {
				fin = listePersonne.size();
			}
			listePersonne = listePersonne.subList(debut, fin);

			mv.setViewName("admin");
			mv.addObject("nbPage", nombrePage);
			mv.addObject("listePersonne", listePersonne);
		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Controller methode post pour l'archivage des messages
	 * 
	 * @param mv       :le modelAndView
	 * @param messages : un tableau des messages à archiver
	 * @param persAuth : la personne authnetifiée
	 * @return le ModelAndView de la boite de reception
	 */
	@RequestMapping(value = "archive", method = RequestMethod.POST)
	public ModelAndView archivePost(ModelAndView mv, @RequestParam(value = "archive") String[] messages,
			@ModelAttribute("persAuth") Personne persAuth) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceMessage iServMes = (IServiceMessage) context.getBean("serviceMessage");
		for (int i = 0; i < messages.length; i++) {
			if(!"".equals(messages[i]) ){
			iServMes.archiverMessage(persAuth.getId(), messages[i]);
			}
		}

		RedirectView redirectView = new RedirectView("/messagerie/boitereception");
		mv.setView(redirectView);

		return mv;
	}

	/**
	 * Controller methode get deconnexion
	 * 
	 * @param mv       :le modelAndView
	 * @param persAuth : la personne authnetifiée
	 * @return le ModelAndView de la page authentification
	 */
	@RequestMapping(value = "/deconnexion", method = RequestMethod.GET)
	public ModelAndView deconnexionGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {

		persAuth = new Personne();
		mv.addObject("persAuth", persAuth);
		RedirectView redirectView = new RedirectView("/messagerie");
		mv.setView(redirectView);
		return mv;
	}

	/**
	 * Controller methode get de la page nouveau message
	 * 
	 * @param mv       : le modelAndView
	 * @param persAuth : la personne authentifiée
	 * @return le ModelAndView remplit de la page nouveau message
	 */
	@RequestMapping(value = "/message/{msg}", method = RequestMethod.GET)
	public ModelAndView affichageMessageGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth,
			@PathVariable String msg) {

		if (persAuth.getId() != 0 && persAuth.isActif()) {
			mv.setViewName("affichageMessage");
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceMessage iServMes = (IServiceMessage) context.getBean("serviceMessage");
			Message message = iServMes.getMessageId(msg);
			if (message != null && (persAuth.getId() == message.getPersonneDest().getId()
					|| persAuth.getId() == message.getPersonneExp().getId())) {
				mv.addObject("message", message);
			}

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode get de la page boite des message archivés
	 * 
	 * @param mv       :le modelAndView
	 * @param persAuth : la personne authentifiée
	 * @return : la boite des message archivé contenant les n 1er messages(5)
	 */
	@RequestMapping(value = "/boitearchive", method = RequestMethod.GET)
	public ModelAndView boiteArchiveGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			mv.setViewName("boitearchive");

			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessageArchive = pers.getMessageRecus().stream().filter(m -> m.getArchiveDest())
					.collect(Collectors.toList());
			listeMessageArchive.addAll(
					pers.getMessageEnvoyees().stream().filter(m -> m.getArchiveExp()).collect(Collectors.toList()));

			listeMessageArchive = listeMessageArchive.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = 0;
			int fin = Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessageArchive.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessageArchive.size()) {
				fin = listeMessageArchive.size();
			}

			List<Message> listeMessageAfficher = listeMessageArchive.subList(debut, fin);
			mv.addObject("listeMessage", listeMessageAfficher);
			mv.addObject("nbPage", nombrePage);

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode get de la page boite des message archivés
	 * 
	 * @param mv       :le modelAndView
	 * @param page     : le numero de page cliqué
	 * @param persAuth : la personne authentifiée
	 * @return : la boite des message archivé contenant les n messages(5) de la
	 *         page
	 */
	@RequestMapping(value = "boitearchive/page", method = RequestMethod.GET, params = { "page" })
	public ModelAndView boiteArchivePageGet(ModelAndView mv, @RequestParam(value = "page") int page,
			@ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessageArchive = pers.getMessageRecus().stream().filter(m -> m.getArchiveDest())
					.collect(Collectors.toList());
			listeMessageArchive.addAll(
					pers.getMessageEnvoyees().stream().filter(m -> m.getArchiveExp()).collect(Collectors.toList()));

			List<Message> listeMessageAfficher = null;
			listeMessageArchive = listeMessageArchive.stream().distinct().collect(Collectors.toList());
			listeMessageArchive = listeMessageArchive.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = page * Constantes.NB_LIGNE - Constantes.NB_LIGNE;
			int fin = page * Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessageArchive.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessageArchive.size()) {
				fin = listeMessageArchive.size();
			}
			listeMessageAfficher = listeMessageArchive.subList(debut, fin);

			mv.setViewName("boitearchive");
			mv.addObject("nbPage", nombrePage);
			mv.addObject("listeMessage", listeMessageAfficher);
		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}
		return mv;
	}

	@RequestMapping(value = "/boiteenvoye", method = RequestMethod.GET)
	public ModelAndView boiteEnvoyeGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			mv.setViewName("boiteenvoye");

			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessage = pers.getMessageEnvoyees();
			listeMessage = listeMessage.stream().filter(m -> !m.getArchiveExp()).collect(Collectors.toList());
			listeMessage = listeMessage.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = 0;
			int fin = Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessage.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessage.size()) {
				fin = listeMessage.size();
			}

			List<Message> listeMessageAfficher = listeMessage.subList(debut, fin);
			mv.addObject("listeMessage", listeMessageAfficher);
			mv.addObject("nbPage", nombrePage);

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode get de la page boite de reception
	 * 
	 * @param mv       :le modelAndView
	 * @param page     : le numero de page cliqué
	 * @param persAuth : la personne authentifiée
	 * @return : la boite de reception contenant les n messages(5) de la page
	 */
	@RequestMapping(value = "boiteenvoye/page", method = RequestMethod.GET, params = { "page" })
	public ModelAndView boiteEnvoyePage(ModelAndView mv, @RequestParam(value = "page") int page,
			@ModelAttribute("persAuth") Personne persAuth) {
		if (persAuth.getId() != 0 && persAuth.isActif()) {
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// recuperation de la personne avec la liste de ses messages
			Personne pers = iServRech.rechercheLogin(persAuth.getAuthentification().getLogin());

			List<Message> listeMessage = pers.getMessageEnvoyees();
			listeMessage = listeMessage.stream().filter(m -> !m.getArchiveExp()).collect(Collectors.toList());
			List<Message> listeMessageAfficher = null;
			listeMessage = listeMessage.stream().sorted((m1, m2) -> m2.getId() - m1.getId())
					.collect(Collectors.toList());

			int debut = page * Constantes.NB_LIGNE - Constantes.NB_LIGNE;
			int fin = page * Constantes.NB_LIGNE;
			int nombreTotalMessage = listeMessage.size();
			int nombrePage = (nombreTotalMessage / Constantes.NB_LIGNE)
					+ (nombreTotalMessage % Constantes.NB_LIGNE != 0 ? 1 : 0);
			if (fin > listeMessage.size()) {
				fin = listeMessage.size();
			}
			listeMessageAfficher = listeMessage.subList(debut, fin);

			mv.setViewName("boiteenvoye");
			mv.addObject("nbPage", nombrePage);
			mv.addObject("listeMessage", listeMessageAfficher);
		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}
		return mv;
	}

	@RequestMapping(value = "/admin/modifier/{login}", method = RequestMethod.GET)
	public ModelAndView modificationAdminUtilisateurGet(ModelAndView mv, @ModelAttribute("persAuth") Personne persAuth,
			@PathVariable String login) {

		if (persAuth.getId() != 0) {
			mv.setViewName("modificationUtilisateur");
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");

			// on envoie l'objet personne au modelandview qui l'envoie � la jsp
			Personne personne = iServRech.rechercheLogin(login);
			mv.addObject("personne", personne);

		} else {
			RedirectView redirectView = new RedirectView("/messagerie");
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller methode post pour l'activation de plusieurs utilisateurs
	 * 
	 * @param mv       :le modelAndView
	 * @param messages : un tableau des messages à archiver
	 * @param persAuth : la personne authnetifiée
	 * @return le ModelAndView de la boite de reception
	 */
	@RequestMapping(value = "/admin/activer", method = RequestMethod.POST)
	public ModelAndView activerUtilisateursPost(ModelAndView mv, @RequestParam(value = "activer") String[] personnes,
			@ModelAttribute("persAuth") Personne persAuth) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		IServiceRecherche iServRech = (IServiceRecherche) context.getBean("serviceRecherche");
		IServicePersonne iServPers = (IServicePersonne) context.getBean("servicePersonne");
		Personne personne = null;
		for (int i = 0; i < personnes.length; i++) {
			personne = iServRech.rechercheLogin(personnes[i]);
			if (personne!=null && !personne.isActif()) {
				iServPers.modificationPersonne(personne.getNom(), personne.getPrenom(), personne.getMail(),
						personne.getTel(), personne.getAdresse(), personne.getDateDeNaissance().toString(),
						personne.getAuthentification().getLogin(), personne.getAuthentification().getMdp(), true);
			}

		}

		RedirectView redirectView = new RedirectView("/messagerie/admin");
		mv.setView(redirectView);

		return mv;
	}
}
