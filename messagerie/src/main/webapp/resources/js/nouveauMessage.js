document.getElementById("form").addEventListener("submit", this.valider);

function valider(event) {
	var eltDest = document.getElementById('destinataire');
    var eltObj = document.getElementById('objet');
    var eltMsg = document.getElementById('message');
	document.getElementById('destErr').innerHTML='';
    document.getElementById('objErr').innerHTML='';
    document.getElementById('msgErr').innerHTML='';
	var retour = true;

	if (eltDest.value.length == 0) {
		document.getElementById('destErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
    } 

    if (eltObj.value.length == 0) {
		document.getElementById('objErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
    } 

    if (eltMsg.value.length == 0) {
		document.getElementById('msgErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
    } 
    

	if (retour) {
		return true;
	} else {
		event.preventDefault();
		return false;
	}
}