document.getElementById("form").addEventListener("submit", this.valider);

function valider(event) {
	var eltLogin = document.getElementById('login');
	var eltMdp = document.getElementById('mdp');
	document.getElementById('loginErr').innerHTML='';
	document.getElementById('mdpErr').innerHTML='';
	var retour = true;

	if (eltLogin.value.length == 0) {
		document.getElementById('loginErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	} else if (eltLogin.value.replace(" ", "").length < eltLogin.value.length) {
		document.getElementById('loginErr').innerHTML = 'pas d\'espace dans le login';
		retour = false;
	} else if (eltLogin.value.includes(',')) {
		document.getElementById('loginErr').innerHTML = 'pas de virgule dans le login';
		retour = false;
	}

	if (eltMdp.value.length == 0) {
		document.getElementById('mdpErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}

	if (retour) {
		return true;
	} else {
		event.preventDefault();
		return false;
	}
}