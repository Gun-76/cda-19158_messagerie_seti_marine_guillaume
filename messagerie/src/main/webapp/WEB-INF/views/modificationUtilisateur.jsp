<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.messagerie.constantes.Constantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>test</title>
</head>
<body>
   <jsp:include page="menu.jsp"/>
<form id="formModification" method="post" action="/messagerie/modification">
<em class="text-danger"> ${KO}</em>
  <div class="form-row">
    <div class="form-group col-md-6">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name="nom" value = "<c:if test="${ not empty personne.nom }"><c:out value="${ personne.nom }"></c:out></c:if>" required>
        <em id="nomErr" class="text-danger" > ${nomKO }</em>
      </div>
      <div class="form-group col-md-6">
        <label for="prenom">Prenom</label>
        <input type="text" class="form-control" id="prenom"  name="prenom" value = "<c:if test="${ not empty personne.prenom }"><c:out value="${ personne.prenom }"></c:out></c:if>" required>
         <em id="prenomErr" class="text-danger" > ${prenomKO }</em>
      </div>
    <div class="form-group col-md-6">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" value = "<c:if test="${ not empty personne.mail }"><c:out value="${ personne.mail }"></c:out></c:if>" required>
      <em id="emailErr" class="text-danger" > ${emailKO }</em>
    </div>
    <div class="form-group col-md-6">
      <label for="tel">Telephone</label>
      <input type="text" class="form-control" id="tel" name="tel" value = "<c:if test="${ not empty personne.tel }"><c:out value="${ personne.tel }"></c:out></c:if>" required>
      <em id="telErr" class="text-danger" >${telKO } </em>
    </div>
    <div class="form-group col-md-6">
      <label for="adresse">Adresse</label>
      <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Adresse" value = "<c:if test="${ not empty personne.adresse }"><c:out value="${ personne.adresse }"></c:out></c:if>" required>
      <em id="adresseErr" class="text-danger">  ${adresseKO }</em>
    </div>
    <div class="form-group col-md-6">
      <label for="dateNaissance">Date de naissance</label>
      <input type="date" class="form-control" id="dateNaissance" name="date_naissance" value = "<c:if test="${ not empty personne.dateDeNaissance }"><c:out value="${ personne.dateDeNaissance }"></c:out></c:if>" required>
       <em id="dateErr" class="text-danger" >${dateKO } </em>
    </div>
    
 
   
    <div class="form-group col-md-6">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" disabled value = "<c:if test="${ not empty personne.authentification.login }"><c:out value="${ personne.authentification.login }"></c:out></c:if>" required>
      </div>
    <div class="form-group col-md-6">
        <label for="mdp">Mot de passe</label>
        <input type="password" class="form-control" id="mdp" name="mdp" value = "<c:if test="${ not empty personne.authentification.mdp }"><c:out value="${ personne.authentification.mdp }"></c:out></c:if>" required> 
        <em id="mdpErr" class="text-danger" >${mdpKO } </em>
      </div>
  </div>
  <div class="form-group">
  <c:if test="${sessionScope.persAuth.role.id eq Constantes.ID_ROLE_ADMIN && personne.role.id!=Constantes.ID_ROLE_ADMIN}">
   <div class="form-group col-md-6">
        <label for="login">Actif : </label>
        <input type="radio" class="" id="actif" name="actif" value="true" <c:if test="${ personne.actif }">checked</c:if>>
        
        <label for="inactif">Inactif :</label>
         <input type="radio" class="" id="inactif" name="actif" value="false" <c:if test="${ !personne.actif }">checked</c:if>>
         
      </div>
    </c:if>
  </div>
  <div class="form-group">
    <input type="text" hidden class=""  name="recuplogin" value="${personne.authentification.login }">
     <c:if test="${!(sessionScope.persAuth.role.id eq Constantes.ID_ROLE_ADMIN && personne.role.id!=Constantes.ID_ROLE_ADMIN)}">
     <input type="text" hidden class=""  name="actif" value="${personne.actif }"></c:if>
  </div>
  
  <button type="submit" class="btn btn-primary" id="valider">Valider</button>
</form>

	
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	
	<script src="${pageContext.request.contextPath}/resources/js/modification.js"></script>
	
	
</body>
</html>
