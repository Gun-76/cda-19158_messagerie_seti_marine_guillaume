<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Messages archiv�s</title>
</head>
<body>
	<jsp:include page="menu.jsp" />

	<div><h1>Messages archiv�s</h1> </div>

	<div class="container-fluid col-sm-12">
		<table class="table table-striped">
			<thead>
				<tr>
				<th>Destinataire</th>
					<th>Expedideur</th>
					<th>Objet</th>
					<th>Date</th>
					<th>details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listeMessage}" var="message">
					<tr>
						<td>${message.personneDest.authentification.login }</td>
						<td>${message.personneExp.authentification.login }</td>
						<td>${message.objet }</td>
						<td>${message.date }</td>
						<td><a href="/messagerie/message/${message.id }">details</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<nav>
	<ul class="pagination">
		<c:forEach var="i" begin="1" end="${ nbPage }" step="1">
					
			<li class="page-item"><a class="page-link" href="/messagerie/boitearchive/page?page=${ i }">${ i }</a></li>	    
					
		</c:forEach>
	</ul>
</nav> 


	</div>





	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>