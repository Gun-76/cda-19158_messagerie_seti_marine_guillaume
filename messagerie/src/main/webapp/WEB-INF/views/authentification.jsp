<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="fr.afpa.messagerie.constantes.Constantes"%>
<!doctype html>
<html>
<title>Connexion</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<body>
	<div class="container text-center">
		<h2>Connexion</h2>
		<br> <br>
		<form id="form" action="" method="post">
			<em class="text-danger"> ${KO}</em>
			<div class="form-group row">
				<label for="login"
					class="col-sm-12 col-md-4 col-lg-4 col-form-label text-right">Login
					:</label>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<input type="text" class="form-control" id="login" name="login">
					<em id="loginErr" class="text-danger"> </em>
				</div>
			</div>
			<div class="form-group row">
				<label for="mdp"
					class="col-sm-12 col-md-4 col-lg-4 col-form-label text-right ">Mot
					de passe : </label>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<input type="password" class="form-control" id="mdp" name="mdp">
					<em id="mdpErr" class="text-danger"> </em>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12 col-md-3 col-lg-3"></div>
				<div class="col-sm-12 col-md-6 col-lg-6">
					<button id="valider" type="submit" class="btn btn-light">Connexion</button>
				</div>
			</div>
		</form>
		<!-- Mettre le lient vers la cr�ation de compte -->
		<a href="creation">Creation d'un nouveau compte</a>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/authentification.js"></script>

</body>
</html>
