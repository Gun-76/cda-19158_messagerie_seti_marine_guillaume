<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<title>Nouveau message</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
      
      <div class="container">
      <div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2"></div>
				<div class="col-sm-12 col-md-8 col-lg-8"><h4>Message : </h4>
				</div>
			</div>
		
		<br>
		<form id="form" action="/messagerie/archive" method="post">
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2"></div>
				<div class="col-sm-12 col-md-8 col-lg-8">
			<b>Destinataire :</b> ${message.personneDest.authentification.login}  ${message.personneDest.nom} ${message.personneDest.prenom}
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2"></div>
				<div class="col-sm-12 col-md-8 col-lg-8">
					<b>Expediteur :</b> ${message.personneExp.authentification.login}  ${message.personneExp.nom} ${message.personneExp.prenom}
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2"></div>
				<div class="col-sm-12 col-md-8 col-lg-8"><b>Objet :</b> ${message.objet} 
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2"></div>
				<div class="col-sm-12 col-md-8 col-lg-8">
					 <pre>${message.message }</pre>
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2">
				</div>
				<div class="col-sm-12 col-md-8 col-lg-8">
				<c:if test="${(!message.archiveDest && message.personneDest.id eq sessionScope.persAuth.id) or (!message.archiveExp && message.personneExp.id eq sessionScope.persAuth.id)}">
					<button type="submit" class="btn btn-light " name="archive" value="${message.id }">Archiver</button>
					</c:if>
				</div>
			</div>
		</form>
		
	</div>
      
         <script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/nouveauMessage.js"></script>
</body>
</html>