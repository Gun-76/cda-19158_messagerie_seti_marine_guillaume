<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>

<div class="container-fluid col-sm-5">
<h4> Felicitations ! Votre compte est cr�e. L'administrateur va l'activer dans quelques instants. </h4>
<a class="nav-item nav-link" href="/messagerie" >Retour</a>
</div>
<script src="js/jquery-3.4.1.slim.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
