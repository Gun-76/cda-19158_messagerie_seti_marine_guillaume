<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<title>Nouveau message</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
      
      <div class="container text-center">
		<div><h3>Nouveau message</h3></div>
		<br> <br>
		<form id="form" action="nouveaumessage" method="post">
			<div class="form-group row">
				<label for="destinataire"
					class="col-sm-12 col-md-2 col-lg-2 col-form-label text-right">Destinataires : </label>
				<div class="col-sm-12 col-md-8 col-lg-8">
					<input type="text" class="form-control" id="destinataire" name="destinataire">
					<em id="destErr" class="text-danger"> </em>
				</div>
			</div>
			<div class="form-group row">
				<label for="mdp"
					class="col-sm-12 col-md-2 col-lg-2 col-form-label text-right ">Objet : </label>
				<div class="col-sm-12 col-md-8 col-lg-8">
					<input type="text" class="form-control" id="objet" name="objet">
					<em id="objErr" class="text-danger"> </em>
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2">
				</div>
				<div class="col-sm-12 col-md-8 col-lg-8">
					 <textarea class="form-control" id="message" name="message" rows="10"></textarea>
					 <em id="msgErr" class="text-danger"> </em>
				</div>
			</div>
			<div class="form-group row">
			<div class="col-sm-12 col-md-2 col-lg-2">
				</div>
				<div class="col-sm-12 col-md-8 col-lg-8">
					<button type="submit" class="btn btn-light ">Envoyer</button>
				</div>
			</div>
		</form>
		
	</div>
      
         <script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/nouveauMessage.js"></script>
</body>
</html>