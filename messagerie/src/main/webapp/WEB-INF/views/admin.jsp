<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Boite de reception</title>
</head>
<body>
	<jsp:include page="menu.jsp" />

	<div><h1>Liste utilisateurs : </h1> </div>
<form action="/messagerie/admin/activer" method="post">
	<div class="container-fluid col-sm-12">
		<table class="table table-striped">
			<thead>
				<tr>
					
					<th>actif</th>
					<th>login</th>
					<th>nom</th>
					<th>prenom</th>
					<th>mail</th>
					<th>date de naissance</th>
					<td></td>
					
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listePersonne}" var="personne">
					<tr>
					<td><input type="checkbox"  <c:if test="${ personne.actif }">checked</c:if> name="activer" value="${personne.authentification.login}"></td>
					
						<td>${personne.authentification.login }</td>
						<td>${personne.nom }</td>
						<td>${personne.prenom }</td>
						<td>${personne.mail }</td>
						<td>${personne.dateDeNaissance }</td>
						<td><a href="/messagerie/admin/modifier/${personne.authentification.login }">modifier</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<nav>
	<!--  --><ul class="pagination">
		<c:forEach var="i" begin="1" end="${ nbPage }" step="1">
					
			<li class="page-item"><a class="page-link" href="/messagerie/admin/page?page=${ i }">${ i }</a></li>	    
					
		</c:forEach>
	</ul>
</nav>
 <input type="text" hidden  name="activer" value=""> 
<button type="submit" class="btn btn-light">Activer</button>
</form>
	</div>





	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>