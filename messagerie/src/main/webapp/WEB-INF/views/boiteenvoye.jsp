<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Boite de reception</title>
</head>
<body>
	<jsp:include page="menu.jsp" />

	<div><h1>Messages envoy�s</h1> </div>

	<div class="container-fluid col-sm-12">
	<form action="/messagerie/archive" method="post">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Archive</th>
					<th>Destinataire</th>
					<th>Objet</th>
					<th>Date</th>
					<th>details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listeMessage}" var="message">
					<tr>
						<td><input type="checkbox" name="archive" value="${message.id}"/></td>
						<td>${message.personneDest.authentification.login }</td>
						<td>${message.objet }</td>
						<td>${message.date }</td>
						<td><a href="/messagerie/message/${message.id }">details</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<nav>
	<ul class="pagination">
		<c:forEach var="i" begin="1" end="${ nbPage }" step="1">
					
			<li class="page-item"><a class="page-link" href="/messagerie/boiteenvoye/page?page=${ i }">${ i }</a></li>	    
					
		</c:forEach>
	</ul>
</nav> 
 <input type="text" hidden  name="archive" value=""> 
<button type="submit" class="btn btn-light">Archiver</button>
</form>
	</div>





	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>