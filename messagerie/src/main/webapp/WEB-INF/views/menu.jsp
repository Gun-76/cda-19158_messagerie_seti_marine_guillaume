<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.messagerie.constantes.Constantes"%>
<!-- 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> -->

    <nav class="navbar navbar-expand-lg  bg-dark ">
        <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
          <ul class="navbar-nav mr-auto">
           <li class="nav-item"> <a class="nav-item nav-link " href="/messagerie/nouveaumessage">Nouveau message</a></li>
            <li class="nav-item"><a class="nav-item nav-link" href="/messagerie/boitereception">Boite de reception</a><li>
            <li class="nav-item"><a class="nav-item nav-link" href="/messagerie/boiteenvoye">Messages envoyes</a><li>
           <li class="nav-item"> <a class="nav-item nav-link " href="/messagerie/boitearchive">Messages archives</a><li>
            <li class="nav-item"><a class="nav-item nav-link " href="/messagerie/modification">Compte</a><li>
            <c:if test="${sessionScope.persAuth.role.id eq Constantes.ID_ROLE_ADMIN }"><li>
           <li class="nav-item"> <a class="nav-item nav-link " href="/messagerie/admin">Admin</a></c:if><li>
            
           <li class="nav-item"> <a class="nav-item nav-link " href="/messagerie/deconnexion">Deconnexion</a><li>
            </ul>
          </div>

      </nav>


<!--
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	
</body>
</body>
</html> -->