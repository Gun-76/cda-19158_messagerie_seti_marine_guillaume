------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------



------------------------------------------------------------
-- Table: role
------------------------------------------------------------
CREATE TABLE public.role_personne(
	id_role   INT  NOT NULL ,
	libelle   VARCHAR (25) NOT NULL  ,
	CONSTRAINT role_PK PRIMARY KEY (id_role)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: personne
------------------------------------------------------------
CREATE TABLE public.personne(
	id_personne      INT  NOT NULL ,
	nom              VARCHAR (25) NOT NULL ,
	prenom           VARCHAR (25) NOT NULL ,
	mail             VARCHAR (50) NOT NULL ,
	tel              VARCHAR (10) NOT NULL ,
	adresse          VARCHAR (255) NOT NULL ,
	date_naissance   DATE  NOT NULL ,
	actif            BOOL  NOT NULL ,
	login            VARCHAR (25) NOT NULL ,
	id_role          INT  NOT NULL  ,
	CONSTRAINT personne_PK PRIMARY KEY (id_personne)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: message
------------------------------------------------------------
CREATE TABLE public.message(
	id_message             INT  NOT NULL ,
	objet                  VARCHAR (100) NOT NULL ,
	message                VARCHAR (255) NOT NULL ,
	date                   timestamp NOT NULL ,
	archiver_exp           BOOL  NOT NULL ,
	archiver_dest          BOOL  NOT NULL ,
	id_exp            	INT  NOT NULL ,
	id_dest  			INT  NOT NULL  ,
	CONSTRAINT message_PK PRIMARY KEY (id_message)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: authentification
------------------------------------------------------------
CREATE TABLE public.authentification(
	login         VARCHAR (25) NOT NULL ,
	mdp           VARCHAR (25) NOT NULL ,
	CONSTRAINT authentification_PK PRIMARY KEY (login)
)WITHOUT OIDS;




ALTER TABLE public.personne
	ADD CONSTRAINT personne_authentification0_FK
	FOREIGN KEY (login)
	REFERENCES public.authentification(login);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_role1_FK
	FOREIGN KEY (id_role)
	REFERENCES public.role_personne(id_role);

ALTER TABLE public.personne 
	ADD CONSTRAINT personne_authentification0_AK 
	UNIQUE (login);

ALTER TABLE public.message
	ADD CONSTRAINT message_personne0_FK
	FOREIGN KEY (id_exp)
	REFERENCES public.personne(id_personne);

ALTER TABLE public.message
	ADD CONSTRAINT message_personne1_FK
	FOREIGN KEY (id_dest)
	REFERENCES public.personne(id_personne);


ALTER TABLE public.personne
	ADD CONSTRAINT personne_desactivation_admin
	CHECK ((id_role=1 and actif=true) or id_role!=1);


INSERT INTO public."role_personne"
(id_role,libelle)
VALUES(1,'administrateur');

INSERT INTO public."role_personne"
(id_role,libelle)
VALUES(2,'utilisateur');

INSERT INTO public.authentification
(login, mdp)
VALUES('admin', '1234');

INSERT INTO public.personne
(id_personne,nom, prenom, mail, tel, adresse, date_naissance, actif, login, id_role)
VALUES(1,'THEBOSS', 'Marine', 'theboss@gmail.com', '0123456789', 'rue de la poup�e qui tousse', '21/11/1990', true, 'admin', 1);

INSERT INTO public.authentification
(login, mdp)
VALUES('user1', '1234');

INSERT INTO public.personne
(id_personne,nom, prenom, mail, tel, adresse, date_naissance, actif, login, id_role)
VALUES(2,'THEQUEEN', 'Seti', 'thequeen@gmail.com', '0123456789', 'rue de la poup�e qui pete', '21/06/1989', true, 'user1', 2);

INSERT INTO public.authentification
(login, mdp)
VALUES('user2', '1234');

INSERT INTO public.personne
(id_personne,nom, prenom, mail, tel, adresse, date_naissance, actif, login, id_role)
VALUES(3,'THETRUANT', 'Guillaume', 'thetruant@gmail.com', '0123456789', 'rue de la poup�e qui ment', '06/11/1976', true, 'user2', 2);
