Projet : Messagerie
Auteur : Marine , Seti, Guillaume

Technologies et outils utilis�s : Java / Maven / Lombok / Hibernate / SPRING / JEE /  Gitkraken / Bitbucket / JMerise / PgAdmin 4 / DBeaver 

D�marche � suivre :

1) Sur pgAdmin ou autre syst�me de gestion de base de donn�es, cr�er un r�le de connexion.

nom = martine
mot de passe = 1234 

2) Sur pgAdmin ou autre syst�me de gestion de base de donn�es, cr�er une nouvelle base de donn�es PostgreSQL(avec l'user martine)

3) Executer le script "Script-bdd_messagerie.sql"( qui se trouve dans le dossier BDD)

4) Mettre messagerie.war dans le dossier web-app du serveur TomCat (version 9)

5) Demarrer l'application avec le serveur TomCat (version 8.5)

	A l'execution du programme vous commencer sur la page d'authentification l'url de cette page est http://localhost:8080/messagerie/

	Vous pouvez soit cr�er un compte un compte (l'administrateur doit activer votre compte avant que vous puissez vous connecter

	soit vous authentifier (pour authentification admin login:admin et le mot de passe:admin)
	
6) Une fois connect� vous allez pouvoir 

	- Envoyer un message �  ou plusieurs destinataires (s�parer les destinataires avec une virgule)

	- Consulter votre boite de reception

	- Consulter les messages envoy�s

	- Consulter les messages archiv�s

	- Archiver les messages
	
	- Modifier vos informations

	- uniquement l'administrateur : g�rer les utilisateurs

	- Se deconnecter


	